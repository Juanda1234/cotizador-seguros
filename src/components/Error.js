import React from 'react';
import styled from '@emotion/styled';

const ErrorDiv = styled.div`
    background-color: red;
    color: white;
    padding: 1rem;
    width: 100%;
    text-align: center;
    margin-bottom: 2rem;
`;

const Error = ({ mensaje}) => (
    <ErrorDiv>
        <span>{mensaje}</span>
    </ErrorDiv>
);

export default Error;